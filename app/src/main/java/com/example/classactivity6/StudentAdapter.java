package com.example.classactivity6;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StudentAdapter extends BaseAdapter {
    Context context;
    HashMap<String,Student> students;
    LayoutInflater test;

    public StudentAdapter(Context applicationContext, HashMap<String, Student> student_list) {
        this.context = applicationContext;
        this.students = student_list;
        this.test = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return students.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {

        // Instantiates a layout XML file into its corresponding View objects.
        view = test.inflate(R.layout.student_output, null);

        // extract the text and image view id from the row.xml
        TextView id = (TextView) view.findViewById(R.id.id);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView age = (TextView) view.findViewById(R.id.age);
        TextView courses = (TextView) view.findViewById(R.id.courses);

        // create array of student keys
        List<String> std_ids = new ArrayList<String>(students.keySet());

        String courseString = "";
        Student std = students.get(std_ids.get(index));

        String[] courseList = std.getCourses();
        // make courses string
        for (int i = 0; i < courseList.length - 1 ; i++) {
            courseString += courseList[i] + ", ";
        }
        courseString += courseList[courseList.length - 1];

        id.setText(std.getId());
        name.setText(std.getName());
        age.setText(Integer.toString(std.getAge()));
        courses.setText(courseString);

        // return the view after setting the icon image and text
        return view;
    }
}
