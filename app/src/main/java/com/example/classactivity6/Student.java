package com.example.classactivity6;

public class Student {
    String id;
    String name;
    int age;
    String Courses[];

    public Student(String id, String name, int age, String[] courses) {
        this.id = id;
        this.name = name;
        this.age = age;
        Courses = courses;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String[] getCourses(){
        return Courses;
    }
}
