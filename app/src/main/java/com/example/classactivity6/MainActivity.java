package com.example.classactivity6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private String url = "https://run.mocky.io/v3/e10cee71-fe98-45db-b663-5569a6752da9";
    ListView student_list;
    HashMap<String, Student> studentList = new HashMap<String, Student>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        student_list = (ListView) findViewById(R.id.listView);

        try {
            new getStudents().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class getStudents extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            StudentAdapter adapter = new StudentAdapter(MainActivity.this, studentList);
            student_list.setAdapter(adapter);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Json Data is downloading",Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            HttpHandler handler = new HttpHandler();

            String jsonString = handler.makeServiceCall(url);

            if (jsonString != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray names = jsonObject.getJSONArray("Students");

                    // putting students into hashmap
                    for (int i = 0; i< names.length(); i++) {
                        JSONObject object = names.getJSONObject(i);
                        JSONArray courseList = object.getJSONArray("Courses");

                        String[] courseArray = new String[courseList.length()];

                        for(int k = 0; k < courseList.length(); k++)
                            courseArray[k] = courseList.getString(k);

                        studentList.put(object.getString("id"), new Student(object.getString("id"), object.getString("name"), object.getInt("age"), courseArray));
                    }

                    System.out.println(studentList.size());

                } catch (JSONException e) {
                    System.out.println(e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Error: " + e.getMessage());
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }

            } else {
                //Log.e(TAG, "Couldn't get json from server.");
                System.out.println("Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }
    }
}